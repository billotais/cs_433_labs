# -*- coding: utf-8 -*-
"""Exercise 3.

Least Square
"""

import numpy as np
from costs import  compute_loss

def least_squares(y, tx):
    """calculate the least squares solution."""
    
    a = tx.T.dot(tx)
    b = tx.T.dot(y)
    weights = np.linalg.solve(a, b)
    mse = compute_loss(y, tx, weights)
    return mse, weights
