# -*- coding: utf-8 -*-
"""implement a polynomial basis function."""

import numpy as np


def build_poly(x, degree):
    """polynomial basis functions for input data x, for j=0 up to j=degree."""

    powers = np.linspace(0, degree, num=degree+1)
    return x.reshape(-1, 1) ** powers
