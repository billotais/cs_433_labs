# -*- coding: utf-8 -*-
"""Exercise 3.

Ridge Regression
"""

import numpy as np
from costs import  compute_loss

def ridge_regression(y, tx, lambda_):
    """implement ridge regression."""
    
    lambda_prime = 2 * tx.shape[0] * lambda_
    a = tx.T.dot(tx) + (lambda_prime * np.eye(tx.shape[1]))
    b = tx.T.dot(y)
    weights = np.linalg.solve(a, b)
    mse = compute_loss(y, tx, weights)
    return mse, weights