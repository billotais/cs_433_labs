# -*- coding: utf-8 -*-
"""Function used to compute the loss."""

def compute_loss(y, tx, w):
    """Calculate the loss.

    You can calculate the loss using mse or mae.
    """
    inner = np.power(y - w[0] - w[1]*tx[:,1],2)
    
    return 1 / (2*tx.shape[0]) * np.sum(inner, axis=0)   